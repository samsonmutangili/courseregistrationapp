package com.example.courseregistrationapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private EditText name;
    private EditText reg_no;
    private Spinner course;
    private Button register;
    private Button exit;
    private String course_selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //reference the widgets
        name = findViewById(R.id.name_edit_text);
        reg_no = findViewById(R.id.regno_edit_text);
        course = findViewById(R.id.course);
        register = findViewById(R.id.register_btn);
        exit = findViewById(R.id.exit_btn);

        //spinner click listener
        course.setOnItemSelectedListener(this);

        //adapter for the courses
        ArrayList<String> courses_list = new ArrayList<>();
        courses_list.add("BSc. Computer Science");
        courses_list.add("BSc. Applied Computer Science");
        courses_list.add("BSc. IT");
        courses_list.add("Diploma in Computer Science");
        courses_list.add("MSc. Computer Science");

        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, courses_list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //attach data to spinner
        course.setAdapter(adapter);

        //set listener for the register button
        register.setOnClickListener(v ->{


            try {
                //get the data from the form

                String student_name = name.getText().toString();
                String student_reg_no = reg_no.getText().toString();

                if(student_name.equals("") || student_name == null){
                    //show error message
                    Toast.makeText(this, "Please enter your name", Toast.LENGTH_SHORT).show();
                } else if(student_reg_no.equals("") || student_reg_no == null){
                    //show error message
                    Toast.makeText(this, "Please enter your registration number", Toast.LENGTH_SHORT).show();
                } else if(course_selected.equals("") || course_selected == null){
                    //show error message
                    Toast.makeText(this, "Please select a course", Toast.LENGTH_SHORT).show();
                } else{


                    Intent intent = new Intent(MainActivity.this, ReceiveData.class);
                    intent.putExtra("student_name", student_name);
                    intent.putExtra("student_reg_no", student_reg_no);
                    intent.putExtra("student_course", course_selected);
                    startActivity(intent);
                }

            } catch (Exception ex){
                Toast.makeText(this, "Error occurred: "+ex, Toast.LENGTH_SHORT).show();
            }

        });

        //click listener for exit button
        exit.setOnClickListener(v->{ finish();});
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        //get the selected item
        course_selected = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}