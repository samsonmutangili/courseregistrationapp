package com.example.courseregistrationapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class ReceiveData extends AppCompatActivity {


    private TextView textView;
    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_data);


        textView = findViewById(R.id.data_tv);

        backButton = findViewById(R.id.back_btn);

        Intent startedIntent = getIntent();
        //get the data from the intent
        String student_name = startedIntent.getStringExtra("student_name").toString();
        String student_reg_no = startedIntent.getStringExtra("student_reg_no").toString();
        String student_course = startedIntent.getStringExtra("student_course").toString();

        String all_data = "Registration successful \nYour name is: "+student_name+"\n Your reg no is: "+student_reg_no
                +"\n Your course is :"+student_course;

        textView.setText(all_data);

        backButton.setOnClickListener(v->{

            Intent intent = new Intent(ReceiveData.this, MainActivity.class);
            this.isDestroyed();
            startActivity(intent);
        });

    }
}